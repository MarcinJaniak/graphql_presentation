# README #

### Queries examples ###
```
{
 "query": "query {trainers {id name job}}"
}
```

```
{
	"query":"{
  __type(name: \"TrainerType\") {
    name
    fields {
      name
      description
      type{ 
      	kind
        name
         ofType {
          name
          kind
        }
      }
      
    }
  }
}"
}
```

```
{
 "query": "query {
  PokeTrainer: trainer(id: 1) {
    name, job
  },
  GymLeader: trainer(id: 2) {
    name, job
  }
}"
}
 
{
	"query": "query {
  trainer(id: 1) {
    id
    name
  }
}"
}
```

```
{
 "query": "query TrainerQuery($trainerId: Int) { trainer(id: $trainerId) { id name } }",
 "variables": "{
   trainerId: 1
 }"
}
```

```
{
"query": "mutation ($gymLeader:GymLeaderInput!){ createGymLeader(gymLeader: $gymLeader) { id name } }",
"variables": "{gymLeader: \r\n{\r\nid: 3,\r\nname : \"Boba fett\"\r\n}}"
}
```

```
{
 "query": "query {trainers 
 {id name job 
			... on TrainerType {
					collectedBadges {
					badgeName imageUrl
									}
								}
}
}"
}
```


```
{
  "query": "query {
  trainerPokemonUnion(name: \"Brock\") {
	... on TrainerBase { name pokemons {name imageUrl}
		... on TrainerType {collectedBadges {badgeName imageUrl}}
		... on GymLeaderType {badge {badgeName imageUrl}}
	}
... on PokemonType {name imageUrl}
  }
}"
}
```
