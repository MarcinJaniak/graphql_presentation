﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLDemo.WebApi.Controllers
{
    public class PokeRepository : IPokeRepository
    {
        #region collections

        private ICollection<Pokemon> _pokemons = new[]
        {
            new Pokemon
            {
                Id = 4,
                Name = "Charmander",
                Attack = 25,
                Level = 5,
                Defense = 25,
                ImageUrl = "https://cdn.bulbagarden.net/upload/b/bb/004MS.png"
            },
            new Pokemon
            {
                Id = 5,
                Name = "Charmeleon",
                Attack = 50,
                Level = 16,
                Defense = 50,
                ImageUrl = "https://cdn.bulbagarden.net/upload/d/dc/005MS.png"
            },
            new Pokemon
            {
                Id = 6,
                Name = "Charizard",
                Attack = 150,
                Level = 36,
                Defense = 120,
                ImageUrl = "https://cdn.bulbagarden.net/upload/0/01/006MS.png"
            },
            new Pokemon
            {
                Id = 25,
                Name = "Pikachu",
                Attack = 25,
                Level = 5,
                Defense = 25,
                ImageUrl = "https://cdn.bulbagarden.net/upload/0/0f/025MS.png"
            },
            new Pokemon
            {
                Id = 74,
                Name = "Geodude",
                Attack = 25,
                Level = 5,
                Defense = 40,
                ImageUrl = "https://cdn.bulbagarden.net/upload/0/04/074MS.png"
            },
            new Pokemon
            {
                Id = 95,
                Name = "Onix",
                Attack = 56,
                Level = 12,
                Defense = 80,
                ImageUrl = "https://cdn.bulbagarden.net/upload/c/cd/095MS.png"
            },
        };

        private ICollection<PokemonBadge> _badges = new[]
        {
            new PokemonBadge
            {
                BadgeName = "Boulder Badge",
                ImageUrl = "https://cdn.bulbagarden.net/upload/thumb/d/dd/Boulder_Badge.png/50px-Boulder_Badge.png"
            },
            new PokemonBadge
            {
                BadgeName = "Cascade Badge",
                ImageUrl = "https://cdn.bulbagarden.net/upload/thumb/9/9c/Cascade_Badge.png/50px-Cascade_Badge.png"
            },
        };

        private ICollection<TrainerBase> _trainers = new List<TrainerBase>
        {
            new Trainer
            {
                Id = 1,
                Name = "Ash",
                CollectedBadges = Enumerable.Empty<PokemonBadge>().ToList(),
                Job = Job.Trainer,
                Pokemons = Enumerable.Empty<Pokemon>().ToList()
            },
            new GymLeader
            {
                Id = 2,
                Name = "Brock",
                Job = Job.GymLeader,
                Badge = null,
                Pokemons = Enumerable.Empty<Pokemon>().ToList()
            }
        };

        #endregion

        public PokeRepository()
        {
            _pokemons.First(x => x.Id == 4).NextEvolution = _pokemons.First(x => x.Id == 5);
            _pokemons.First(x => x.Id == 5).NextEvolution = _pokemons.First(x => x.Id == 6);

            _trainers.First(x => x.Id == 1).Pokemons.Add(_pokemons.First(x => x.Id == 4));
            _trainers.First(x => x.Id == 1).Pokemons.Add(_pokemons.First(x => x.Id == 25));
            ((Trainer) _trainers.First(x => x.Id == 1)).CollectedBadges.Add(_badges.First(x =>
                x.BadgeName == "Cascade Badge"));

            _trainers.First(x => x.Id == 2).Pokemons.Add(_pokemons.First(x => x.Id == 74));
            _trainers.First(x => x.Id == 2).Pokemons.Add(_pokemons.First(x => x.Id == 95));
            ((GymLeader) _trainers.First(x => x.Id == 2)).Badge =
                _badges.First(x => x.BadgeName == "Boulder Badge");
        }

        public Task<ICollection<TrainerBase>> Get(Job job = Job.Any)
        {
            return job == Job.Any
                ? Task.FromResult(_trainers)
                : Task.FromResult(((ICollection<TrainerBase>) _trainers.Where(x => x.Job == job)));
        }

        public Task<TrainerBase> Get(int id)
        {
            return Task.FromResult(_trainers.FirstOrDefault(trainer => trainer.Id == id));
        }

        public Task<TrainerBase> GetTrainerByName(string name)
        {
            return Task.FromResult(_trainers.FirstOrDefault(trainer => trainer.Name == name));
        }

        public Task<Pokemon> GetPokemonByName(string name)
        {
            return Task.FromResult(_pokemons.FirstOrDefault(trainer => trainer.Name == name));
        }

        public GymLeader AddGymLeader(GymLeader gymLeader)
        {
            _trainers.Add(gymLeader);
            return gymLeader;
        }
    }
}