﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GraphQLDemo.WebApi.Controllers
{
    public interface IPokeRepository
    {
        Task<ICollection<TrainerBase>> Get(Job job = Job.Any);
        Task<TrainerBase> Get(int id);
        Task<TrainerBase> GetTrainerByName(string name);
        Task<Pokemon> GetPokemonByName(string name);

        GymLeader AddGymLeader(GymLeader gymLeader);
    }
}