﻿using System;
using GraphQL.Types;

namespace GraphQLDemo.WebApi.Controllers
{
    public class TrainerBaseType : InterfaceGraphType<TrainerBase>
    {
        public TrainerBaseType()
        {
            Name = "TrainerBase";
            Field(x => x.Id, nullable: false);
            Field(x => x.Name, nullable: false);
            Field<JobEnum>("job");
            Field<ListGraphType<PokemonType>>("pokemons");
        }

        public TrainerBaseType(TrainerType trainerType, GymLeaderType gymLeaderType) : base()
        {
            ResolveType = obj =>
            {
                if (obj is Trainer)
                {
                    return trainerType;
                }

                if (obj is GymLeader)
                {
                    return gymLeaderType;
                }

                throw new ArgumentOutOfRangeException($"Could not resolve graph type for {obj.GetType().Name}");
            };
        }
        
    }
    
      public class TrainerType : ObjectGraphType<Trainer>
    {
        public TrainerType()
        {
            Field<ListGraphType<PokemonBadgeType>>("collectedBadges");

            Field(x => x.Id, nullable: false);
            Field(x => x.Name, nullable: false);
            Field<JobEnum>("job");
            Field<ListGraphType<PokemonType>>("pokemons");

            Interface<TrainerBaseType>();
            
            //IsTypeOf = obj => obj is TrainerBase;
        }
    }

    public class JobEnum : EnumerationGraphType
    {
        public JobEnum()
        {
            Name = "job";
            Description = "Trainer profession.";
            AddValue("GYMLEADER", "Gym leader.", 2);
            AddValue("TRAINER", "Trainer", 1);
        }
    }

    public class PokemonBadgeType : ObjectGraphType<PokemonBadge>
    {
        public PokemonBadgeType()
        {
            Field(x => x.BadgeName, nullable: false).Description("Badge name");
            Field(x => x.ImageUrl, nullable: false).Description("Badge awarded after defeating him");
        }
    }

    public class GymLeaderType : ObjectGraphType<GymLeader>
    {
        public GymLeaderType()
        {
            Field<PokemonBadgeType>("badge");

            Field(x => x.Id, nullable: false);
            Field(x => x.Name, nullable: false);
            Field<JobEnum>("job");
            Field<ListGraphType<PokemonType>>("pokemons");

            Interface<TrainerBaseType>();
            
            //IsTypeOf = obj => obj is TrainerBase;

        }
    }

    public class PokemonType : ObjectGraphType<Pokemon>
    {
        public PokemonType()
        {
            Field(x => x.Id, nullable: false).Description("PokeId");
            Field(x => x.Name, nullable: false).Description("PokeName");
            Field<PokemonType>("nextEvolution");

            Field(x => x.Attack).Description("Poke attack");
            Field(x => x.Defense).Description("Poke defense");
            Field(x => x.Level);
            Field(x => x.ImageUrl);
        }
    }

    public class TrainerOrPokemonType : UnionGraphType
    {
        public TrainerOrPokemonType()
        {

            Type<TrainerType>();
            Type<GymLeaderType>();
            Type<PokemonType>();
            
        }
    }
   
    public class GymLeaderInputType : InputObjectGraphType
    {
        public GymLeaderInputType()
        {
            Name = "GymLeaderInput";

            Field<NonNullGraphType<StringGraphType>>("name");
            Field<NonNullGraphType<IntGraphType>>("id");
        }
    }
}