﻿using System.Collections.Generic;

namespace GraphQLDemo.WebApi.Controllers
{
    public abstract class TrainerBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Job Job { get; set; }
        public ICollection<Pokemon> Pokemons { get; set; }
    }
    
    public class Trainer : TrainerBase
    {
        public ICollection<PokemonBadge> CollectedBadges { get; set; }
    }

    public class GymLeader : TrainerBase
    {
        public PokemonBadge Badge { get; set; }
    }

    public enum Job
    {
        Any,
        Trainer,
        GymLeader
    }

    public class PokemonBadge
    {
        public string BadgeName { get; set; }
        public string ImageUrl { get; set; }
    }

    public class Pokemon
    {
        public int Id { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }

        public string ImageUrl { get; set; }

        public Pokemon NextEvolution { get; set; }
    }

}