﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using GraphQL.Validation;
using GraphQL.Validation.Complexity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp;

namespace GraphQLDemo.WebApi.Controllers
{
    public class PokeMutation : ObjectGraphType<object>
    {
        public PokeMutation(IPokeRepository pokeRepository)
        {
            Name = "Mutation";

            Field<GymLeaderType>(
                "createGymLeader",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<GymLeaderInputType>> {Name = "gymLeader"}
                ),
                resolve: context =>
                {
                    var gymLeader = context.GetArgument<GymLeader>("gymLeader");
                    return pokeRepository.AddGymLeader(gymLeader);
                });
        }
    }

    public class PokeQuery : ObjectGraphType
    {
        private readonly IPokeRepository _pokeRepository;

        public PokeQuery(IPokeRepository pokeRepository)
        {
            _pokeRepository = pokeRepository;

            Field<TrainerBaseType>("trainer",
                arguments:
                new QueryArguments(new QueryArgument<IntGraphType>
                {
                    Name = "id"
                }),
                resolve:
                context =>
                {
                    var id = context.GetArgument<string>("id");
                    var intId = int.Parse(id);
                    return pokeRepository.Get(intId);
                });

            Field<TrainerOrPokemonType>("trainerPokemonUnion",
                arguments:
                new QueryArguments(new QueryArgument<StringGraphType>
                {
                    Name = "name"
                }),
                resolve:
                context =>
                {
                    var name = context.GetArgument<string>("name");


                    var poke = pokeRepository.GetPokemonByName(name);
                    var trainer = pokeRepository.GetTrainerByName(name);

                    if (poke.Result != null)
                    {
                        return poke;
                    }

                    return trainer;
                });

            Field<ListGraphType<TrainerBaseType>>("trainers", resolve:
                context => pokeRepository.Get());
        }
    }

    public class GraphQLQuery
    {
        public string OperationName { get; set; }
        public string NamedQuery { get; set; }
        public string Query { get; set; }
        public string Variables { get; set; }
    }


    [Route("PokeQL")]
    public class TrainerController : Controller
    {
        private IPokeRepository _pokeRepository;

        public TrainerController(IPokeRepository pokeRepository)
        {
            _pokeRepository = pokeRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            var schema = new Schema
            {
                Query = new PokeQuery(_pokeRepository),
                Mutation = new PokeMutation(_pokeRepository)
            };
            schema.RegisterType<TrainerType>();
            schema.RegisterType<GymLeaderType>();
            var inputs = query.Variables.ToInputs();

            var result = await new DocumentExecuter().ExecuteAsync(x =>
            {
                
               // x.ComplexityConfiguration = new ComplexityConfiguration();
                //x.ComplexityConfiguration.MaxDepth = 5;
                //x.ComplexityConfiguration.MaxComplexity = 150;
                //x.ComplexityConfiguration.FieldImpact = 150;

                x.Schema = schema;
                x.Query = query.Query;
                x.OperationName = query.OperationName;
                x.Inputs = inputs;
            }).ConfigureAwait(false);

            if (result.Errors?.Count > 0)
            {
                return BadRequest();
            }

            return Ok(result);
        }
    }
}