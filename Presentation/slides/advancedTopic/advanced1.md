#### Advanced topics consists:
1. Error handling
1. User Context
1. Dependency injection
1. Object/field metadata
1. Field middleware
1. Auth
1. Protection
1. Query batching
1. Metrics

[More here](https://graphql-dotnet.github.io/learn/ )
 