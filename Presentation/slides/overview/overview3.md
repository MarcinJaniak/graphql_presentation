## Basic sample

##### Query:
```json
{
  me {
       name
     }
   }
   ```
   
   ##### Sample result:
   ```json
{
  "me": {
    "name": "Luke Skywalker"
  }
}
```