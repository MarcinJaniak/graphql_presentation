# Sources and useful links

1. DOCS: https://graphql-dotnet.github.io/
1. Official site: http://graphql.org/
1. JSON formatter: https://jsonformatter.curiousconcept.com/
