#### No patterns, no rules
   
```javascript
<script>
var db = openDatabase('myDinosaurs', '1.0', 'hello_palaeolithic',
                                                2 * 1024 * 1024);
db.transaction(function (tx) {
  tx.executeSql('CREATE TABLE IF NOT EXISTS
                        dinosaurs (id unique, text)');
  tx.executeSql('INSERT INTO dinosaurs
                       (id, text) VALUES (1, "triceratops")');
});
</script>
```